// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthDisplayer.generated.h"


UCLASS( Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOONTANKS_API UHealthDisplayer : public UActorComponent
{
    GENERATED_BODY()

public:
    UHealthDisplayer();

    UFUNCTION(BlueprintImplementableEvent)
    void HealthUpdated(float HealthUpdatePercentage);
    UFUNCTION(BlueprintImplementableEvent)
    void SetupWidget(APlayerController* PlayerController);
    void PlayerControllerPossessed(APlayerController* PlayerController);

protected:
    // Called when the game starts
    virtual void BeginPlay() override;
};
