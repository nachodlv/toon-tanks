// Fill out your copyright notice in the Description page of Project Settings.


#include "Destructible.h"

#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/Classes/Sound/SoundBase.h"
#include "Engine/Classes/Particles/ParticleSystem.h"
#include "Engine/Classes/Camera/CameraShake.h"

UDestructible::UDestructible()
{
	PrimaryComponentTick.bCanEverTick = false;

	static ConstructorHelpers::FObjectFinder<USoundBase> DeathSoundFinder(TEXT("/Game/Assets/Audio/Explode_Audio"));
	DeathSound = DeathSoundFinder.Object;

	static ConstructorHelpers::FObjectFinder<UParticleSystem> DeathParticleFinder(TEXT("/Game/Assets/Effects/P_DeathEffect"));
	DeathParticle = DeathParticleFinder.Object;

	static ConstructorHelpers::FClassFinder<UCameraShake> CameraShakeFinder(TEXT("/Game/Blueprints/General/BP_CamShakeExplode"));
	DeathShake = CameraShakeFinder.Class;
}

void UDestructible::Destroy_Implementation() const
{
	AActor* Actor = GetOwner();
	if(!Actor) return;
	if (DeathParticle)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, DeathParticle, Actor->GetActorLocation(), FRotator::ZeroRotator,
                                                 DeathParticleScale);
	}
	if (DeathSound) UGameplayStatics::PlaySoundAtLocation(this, DeathSound, Actor->GetActorLocation());
	if (DeathShake)
	{
		for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
		{
			APlayerController* PlayerController = Iterator->Get();
			if(PlayerController && PlayerController->IsLocalController())
				PlayerController->PlayerCameraManager->PlayCameraShake(DeathShake, 1);
		}
	}
}

bool UDestructible::Destroy_Validate()
{
	return true;
}
