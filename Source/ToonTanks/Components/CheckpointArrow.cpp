// Fill out your copyright notice in the Description page of Project Settings.


#include "CheckpointArrow.h"

UCheckpointArrow::UCheckpointArrow()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UCheckpointArrow::ShowArrow_Implementation(const FVector Checkpoint)
{
	if (!ArrowMeshComponent) return;
	ArrowMeshComponent->SetHiddenInGame(false);
	NextCheckpoint = Checkpoint;
}

bool UCheckpointArrow::ShowArrow_Validate(const FVector Checkpoint)
{
	return true;
}

void UCheckpointArrow::HideArrow_Implementation() const
{
	ArrowMeshComponent->SetHiddenInGame(true);
}

bool UCheckpointArrow::HideArrow_Validate()
{
	return true;
}

void UCheckpointArrow::SetArrowStaticMesh(UStaticMeshComponent* ArrowMesh)
{
	ArrowMeshComponent = ArrowMesh;
}

void UCheckpointArrow::TickComponent(float DeltaTime, ELevelTick TickType,
                                     FActorComponentTickFunction* ThisTickFunction)
{
	if (ArrowMeshComponent && !ArrowMeshComponent->bHiddenInGame)
	{
		const auto ActorLocation = GetOwner()->GetActorLocation();
		FVector Direction = NextCheckpoint - ActorLocation;
		Direction.Normalize();
		const FVector Position = ActorLocation + Direction * ArrowDistance;
		SetWorldLocation(Position);
		const auto ActorRotation = GetComponentRotation();
		FRotator Rotator = FRotationMatrix::MakeFromX(Direction.GetSafeNormal()).Rotator();
		Rotator.Roll = ActorRotation.Roll;
		Rotator.Pitch = ActorRotation.Pitch;
		SetWorldRotation(Rotator);
	}
}
