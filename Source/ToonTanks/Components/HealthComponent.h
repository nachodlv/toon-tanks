// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ToonTanks/GameModes/TankGameModeBase.h"

#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FHealthUpdated, float, HealthRemainingPercetage);

UCLASS( Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOONTANKS_API UHealthComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    UHealthComponent();

    UPROPERTY(BlueprintCallable)
    FHealthUpdated OnHealthUpdated;

    void Heal(float HealAmount);

protected:
    virtual void BeginPlay() override;

    UFUNCTION()
    void TakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, class AController* InstigatedBy,
                    AActor* DamageActor);

private:
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Health", meta = (AllowPrivateAccess = "true"))
    float DefaultHealth = 100.0f;
    UPROPERTY(ReplicatedUsing=OnRep_Health)
    float Health = 0.0f;

    UPROPERTY()
    AActor* Owner;
    UPROPERTY()
    ATankGameModeBase* GameMode;

    UFUNCTION()
    void OnRep_Health();
};
