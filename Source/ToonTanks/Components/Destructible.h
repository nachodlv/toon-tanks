// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Camera/CameraShake.h"
#include "Components/ActorComponent.h"
#include "Destructible.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOONTANKS_API UDestructible : public UActorComponent
{
	GENERATED_BODY()

public:
	UDestructible();
	UFUNCTION(NetMulticast, Reliable, WithValidation)
	void Destroy() const;

private:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Effects", meta = (AllowPrivateAccess = "true"))
	UParticleSystem* DeathParticle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Effects", meta = (AllowPrivateAccess = "true"))
	FVector DeathParticleScale = FVector::OneVector;
	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category = "Effects", meta = (AllowPrivateAccess = "true"))
	USoundBase* DeathSound;
	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category = "Effects", meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UCameraShake> DeathShake;

};
