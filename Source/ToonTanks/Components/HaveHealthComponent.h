﻿#pragma once
#include "HaveHealthComponent.generated.h"

class UHealthComponent;
UINTERFACE(MinimalAPI)
class UHaveHealthComponent : public UInterface
{
    GENERATED_BODY()
};

class IHaveHealthComponent
{
    GENERATED_BODY()

public:
    virtual UHealthComponent* GetHealthComponent() {return nullptr;}
};
