﻿#pragma once
#include "HaveCheckpointArrow.generated.h"

class UCheckpointArrow;
UINTERFACE(MinimalAPI)
class UHaveCheckpointArrow : public UInterface
{
    GENERATED_BODY()
};

class IHaveCheckpointArrow
{
    GENERATED_BODY()

public:
    virtual UCheckpointArrow* GetCheckpointArrow() {return nullptr;}
};
