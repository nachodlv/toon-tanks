// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthDisplayer.h"


#include "HaveHealthComponent.h"
#include "HealthComponent.h"

UHealthDisplayer::UHealthDisplayer()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UHealthDisplayer::PlayerControllerPossessed(APlayerController* PlayerController)
{
	if(!PlayerController) return;

	if(!PlayerController) return;
	SetupWidget(PlayerController);
	auto HaveHealthComponent = Cast<IHaveHealthComponent>(PlayerController->GetPawn());
	if(HaveHealthComponent)
	{
		UHealthComponent* HealthComponent = HaveHealthComponent->GetHealthComponent();
		if(HealthComponent)
		{
			HealthComponent->OnHealthUpdated.AddDynamic(this, &UHealthDisplayer::HealthUpdated);
			return;
		}
	}

	UE_LOG(LogTemp, Error, TEXT("Health displayer has no reference to a Health Component"));
}

void UHealthDisplayer::BeginPlay()
{
	Super::BeginPlay();
}




