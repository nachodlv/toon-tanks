// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "ScoreDisplayer.generated.h"

class UGameInstanceBase;


class APlayerControllerBase;
class APlayerStateBase;
UCLASS( Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TOONTANKS_API UScoreDisplayer : public UActorComponent
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintImplementableEvent)
    void UpdateMultiplier(int Multiplier);
    UFUNCTION(BlueprintImplementableEvent)
    void UpdatePlayerScore(float Score);
    UFUNCTION(BlueprintImplementableEvent)
    void SetupWidgetFromBlueprint(APlayerController* NewPlayerController);

    UFUNCTION(BlueprintCallable)
    int GetScore() const;
    UFUNCTION(BlueprintCallable)
    int GetMultiplier() const;
    UFUNCTION(BlueprintCallable)
    float GetComboTimeRemaining() const;
    UFUNCTION(BlueprintCallable)
    float GetPercentageOfTimeLeft() const;
    void PlayerControllerPossessed(APlayerControllerBase* NewPlayerController);

private:
    UPROPERTY()
    APlayerControllerBase* PlayerController;
    UPROPERTY()
    APlayerStateBase* PlayerState;
};
