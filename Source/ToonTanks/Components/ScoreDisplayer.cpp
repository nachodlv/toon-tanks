// Fill out your copyright notice in the Description page of Project Settings.


#include "ScoreDisplayer.h"

#include "ToonTanks/PlayerStates/PlayerStateBase.h"
#include "ToonTanks/PlayerControllers/PlayerControllerBase.h"

void UScoreDisplayer::PlayerControllerPossessed(APlayerControllerBase* NewPlayerController)
{
    if (!NewPlayerController) return;
    PlayerController = NewPlayerController;
    PlayerState = PlayerController->GetPlayerState<APlayerStateBase>();
    if (PlayerState)
    {
        PlayerState->OnUpdateMultiplier.AddDynamic(this, &UScoreDisplayer::UpdateMultiplier);
        PlayerState->OnUpdateScore.AddDynamic(this, &UScoreDisplayer::UpdatePlayerScore);
    }
    RegisterComponent();
    SetupWidgetFromBlueprint(NewPlayerController);
}

int UScoreDisplayer::GetScore() const
{
    if (PlayerState) return PlayerState->GetScore();
    return 0;
}

int UScoreDisplayer::GetMultiplier() const
{
    if (PlayerState) return PlayerState->GetMultiplier();
    return 0;
}

float UScoreDisplayer::GetComboTimeRemaining() const
{
    if (PlayerState) return PlayerState->GetComboTimeRemaining();
    return 0;
}

float UScoreDisplayer::GetPercentageOfTimeLeft() const
{
    if (PlayerState)
        return GetComboTimeRemaining() / PlayerState->GetTimeToLoseCombo();
    return 0;
}
