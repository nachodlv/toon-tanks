// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"

#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "ToonTanks/GameModes/TankGameModeBase.h"

UHealthComponent::UHealthComponent()
{
    PrimaryComponentTick.bCanEverTick = false;
}

void UHealthComponent::Heal(const float HealAmount)
{
    const auto NewHealth = Health + HealAmount;
    if (NewHealth > DefaultHealth) Health = DefaultHealth;
    else Health = NewHealth;
}

void UHealthComponent::BeginPlay()
{
    Super::BeginPlay();

    Health = DefaultHealth;

    GameMode = Cast<ATankGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
    Owner = GetOwner();
    if (Owner)
    {
        Owner->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::TakeDamage);
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("Health component has no reference to an Owner"));
    }
}

void UHealthComponent::TakeDamage(AActor* DamagedActor, const float Damage, const UDamageType* DamageType,
                                  AController* InstigatedBy, AActor* DamageActor)
{
    if (Damage == 0 || Health == 0) return;
    Health = FMath::Clamp(Health - Damage, 0.0f, DefaultHealth);
    OnHealthUpdated.Broadcast(Health / DefaultHealth);
    if (Health > 0) return;
    if (GameMode)
    {
        GameMode->ActorDie(Owner, InstigatedBy ? InstigatedBy->GetPawn() : nullptr);
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("Health component has no reference to the GameMode"));
    }
}

void UHealthComponent::OnRep_Health()
{
    OnHealthUpdated.Broadcast(Health / DefaultHealth);
}

void UHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);
    DOREPLIFETIME(UHealthComponent, Health);
}
