// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CheckpointArrow.generated.h"

UCLASS( Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TOONTANKS_API UCheckpointArrow : public USceneComponent
{
	GENERATED_BODY()


public:
	UCheckpointArrow();

	UFUNCTION(Client, Reliable, WithValidation)
	void ShowArrow(const FVector Checkpoint);
	UFUNCTION(Client, Reliable, WithValidation)
	void HideArrow() const;
	void SetArrowStaticMesh(UStaticMeshComponent* ArrowMesh);

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Arrow", meta = (AllowPrivateAccess = "true"))
	float ArrowDistance = 200.f;

	UPROPERTY()
	UStaticMeshComponent* ArrowMeshComponent;

	FVector NextCheckpoint;

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType,
	                           FActorComponentTickFunction* ThisTickFunction) override;
};
