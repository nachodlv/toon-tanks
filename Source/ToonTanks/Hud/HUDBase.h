// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "HUDBase.generated.h"

/**
 *
 */
UCLASS()
class TOONTANKS_API AHUDBase : public AHUD
{
    GENERATED_BODY()

public:
    UFUNCTION(BlueprintImplementableEvent)
    void GameStart(int StartDelay);
    UFUNCTION(BlueprintImplementableEvent)
    void GameOver(bool PlayerWon);
};
