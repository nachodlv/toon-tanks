// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthInteractable.h"


#include "Components/ShapeComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/BillboardComponent.h"
#include "ToonTanks/Components/HealthComponent.h"
#include "ToonTanks/Pawn/PawnTank.h"

AHealthInteractable::AHealthInteractable()
{
    PrimaryActorTick.bCanEverTick = true;
}

void AHealthInteractable::BeginPlay()
{
    Super::BeginPlay();

    OnActorBeginOverlap.AddDynamic(this, &AHealthInteractable::InteractableHit);
    if(UBillboardComponent* BillboardComponent = GetSpriteComponent())
    {
        BillboardComponent->SetHiddenInGame(true);
    }
}

void AHealthInteractable::InteractableHit(AActor* OverlappedActor, AActor* OtherActor)
{
    if (APawnTank* Tank = Cast<APawnTank>(OtherActor))
    {
        auto HealthComponent = Tank->GetHealthComponent();
        if (!HealthComponent) return;
        HealthComponent->Heal(HealAmount);
        ShowEffects();
        if (OverlappedActor)
        {
            OverlappedActor->SetActorTickEnabled(false);
            OverlappedActor->SetActorHiddenInGame(true);
            if (auto Collider = GetCollisionComponent())
            {
                Collider->SetCollisionEnabled(ECollisionEnabled::NoCollision);
            }
        }
    }
}

void AHealthInteractable::ShowEffects() const
{
    if (HealParticleSystem)
    {
        const auto ParticlePosition = GetActorLocation() + ParticlePositionOffset;
        UParticleSystemComponent* ParticleComponent = UGameplayStatics::SpawnEmitterAtLocation(
            this, HealParticleSystem, ParticlePosition, FRotator::ZeroRotator,
            ParticleScale);
        if(ParticleComponent) ParticleComponent->CustomTimeDilation = ParticleSpeed;
    }
}

void AHealthInteractable::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}
