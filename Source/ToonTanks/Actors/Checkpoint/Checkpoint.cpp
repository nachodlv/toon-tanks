// Fill out your copyright notice in the Description page of Project Settings.


#include "Checkpoint.h"

#include "ToonTanks/Pawn/PawnTank.h"

ACheckpoint::ACheckpoint()
{
    OnActorBeginOverlap.AddDynamic(this, &ACheckpoint::CheckpointGrabbed);
}

void ACheckpoint::CheckpointGrabbed(AActor* OverlappedActor, AActor* OtherActor)
{
    if(!HasAuthority()) return;
    if(Cast<APawnTank>(OtherActor))
    {
        OnCheckpointGrabbed.Broadcast(this);
    }
}


