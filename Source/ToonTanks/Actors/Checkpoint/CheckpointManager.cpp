#include "CheckpointManager.h"
#include "Checkpoint.h"
#include "Kismet/GameplayStatics.h"
#include "ToonTanks/Components/CheckpointArrow.h"
#include "ToonTanks/Components/HaveCheckpointArrow.h"
#include "ToonTanks/Pawn/PawnTank.h"
#include "GameFramework/PlayerController.h"

ACheckpointManager::ACheckpointManager()
{
    PrimaryActorTick.bCanEverTick = true;
}

void ACheckpointManager::BeginPlay()
{
    Super::BeginPlay();
    if(!HasAuthority()) return;
    for (auto Checkpoint : Checkpoints)
    {
        Checkpoint->OnCheckpointGrabbed.AddDynamic(this, &ACheckpointManager::CheckpointGrabbed);
    }
}

void ACheckpointManager::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    const float Now = UGameplayStatics::GetRealTimeSeconds(GetWorld());
    if (Now - LastCheckpointGrabbed > TimeToShowArrow)
    {
        ShowArrow();
    }
}

void ACheckpointManager::CheckpointGrabbed(ACheckpoint* ACheckpoint)
{
    for (int32 i = 0; i < Checkpoints.Num(); ++i)
    {
        UE_LOG(LogTemp, Warning, TEXT("Checkpoint grabbed"));
        if (ACheckpoint != Checkpoints[i])
            continue;
        if (i + 1 != Checkpoints.Num()) NextCheckpoint = i + 1;
        HideArrow();
        return;
    }
}

void ACheckpointManager::ShowArrow()
{
    if(ShowingArrow) return;
    if (Checkpoints.Num() <= NextCheckpoint) return;
    ShowingArrow = true;
    const auto CheckpointPosition = Checkpoints[NextCheckpoint]->GetActorLocation();
    GetPawnsInGame(PawnTanks);
    for (APawnTank* Pawn : PawnTanks)
    {
        if (UCheckpointArrow* CheckpointArrow = Pawn->GetCheckpointArrow())
        {
            CheckpointArrow->ShowArrow(CheckpointPosition);
        }
    }
}

void ACheckpointManager::HideArrow()
{
    if(!ShowingArrow) return;
    ShowingArrow = false;
    LastCheckpointGrabbed = UGameplayStatics::GetRealTimeSeconds(GetWorld());
    for (auto PawnTank : PawnTanks)
    {
        if (UCheckpointArrow* CheckpointArrow = PawnTank->GetCheckpointArrow())
        {
            CheckpointArrow->HideArrow();
        }
    }
    PawnTanks.Empty();
}

void ACheckpointManager::GetPawnsInGame(TArray<APawnTank*>& Pawns) const
{
    FConstPlayerControllerIterator PlayerControllers = GetWorld()->GetPlayerControllerIterator();
    for (; PlayerControllers; ++ PlayerControllers)
    {
        if (APawnTank* PlayerPawn = Cast<APawnTank>(PlayerControllers->Get()->GetPawn()))
        {
            Pawns.Add(PlayerPawn);
        }
    }
}
