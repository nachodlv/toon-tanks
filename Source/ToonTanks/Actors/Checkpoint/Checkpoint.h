// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "Checkpoint.generated.h"

class ACheckpoint;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCheckpointGrabbed, ACheckpoint*, Checkpoint);

UCLASS()
class TOONTANKS_API ACheckpoint : public ATriggerBox
{
    GENERATED_BODY()

public:
    ACheckpoint();
    FCheckpointGrabbed OnCheckpointGrabbed;

private:
    UFUNCTION()
    void CheckpointGrabbed(AActor* OverlappedActor, AActor* OtherActor);
};
