// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "CheckpointManager.generated.h"

class ACheckpoint;
class UCheckpointArrow;
class APawnTank;

class IHaveCheckpointArrow;
UCLASS()
class TOONTANKS_API ACheckpointManager : public AActor
{
    GENERATED_BODY()

public:
    ACheckpointManager();
    virtual void Tick(float DeltaTime) override;

protected:
    virtual void BeginPlay() override;

private:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "References", meta = (AllowPrivateAccess = "true"))
    TArray<ACheckpoint*> Checkpoints;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Timer", meta = (AllowPrivateAccess = "true"))
    float TimeToShowArrow = 1;

    UPROPERTY()
    TArray<APawnTank*> PawnTanks;

    int32 NextCheckpoint = 0;
    bool ShowingArrow = false;
    float LastCheckpointGrabbed = 0;

    UFUNCTION()
    void CheckpointGrabbed(ACheckpoint* ACheckpoint);
    void ShowArrow();
    void HideArrow();
    void GetPawnsInGame(TArray<APawnTank*>& Pawns) const;
};
