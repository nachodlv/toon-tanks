// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "HealthInteractable.generated.h"

UCLASS()
class TOONTANKS_API AHealthInteractable : public ATriggerBox
{
    GENERATED_BODY()

public:
    // Sets default values for this actor's properties
    AHealthInteractable();
    virtual void BeginPlay() override;

private:
    UPROPERTY(EditAnywhere, BlueprintReadWrite,Category = "Heal", meta = (AllowPrivateAccess = "true"))
    float HealAmount;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects", meta = (AllowPrivateAccess = "true"))
    UParticleSystem* HealParticleSystem;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects", meta = (AllowPrivateAccess = "true"))
    FVector ParticleScale = FVector::OneVector;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects", meta = (AllowPrivateAccess = "true"))
    float ParticleSpeed = 1.5f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects", meta = (AllowPrivateAccess = "true"))
    FVector ParticlePositionOffset = FVector::OneVector;

    UFUNCTION()
    void InteractableHit(AActor* OverlappedActor, AActor* OtherActor);
    void ShowEffects() const;
    virtual void Tick(float DeltaTime) override;
};
