// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileBase.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMeshActor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"

// Sets default values
AProjectileBase::AProjectileBase()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = false;
    ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Projectile Mesh"));
    ProjectileMesh->OnComponentHit.AddDynamic(this, &AProjectileBase::OnHit);
    RootComponent = ProjectileMesh;

    ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));
    ProjectileMovement->InitialSpeed = MovementSpeed;
    ProjectileMovement->MaxSpeed = MovementSpeed;

    ParticleTrail = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Particle Trail"));
    ParticleTrail->SetupAttachment(RootComponent);

    InitialLifeSpan = 3;
}

void AProjectileBase::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherCompo,
                            FVector NormalImpulse, const FHitResult& Result)
{
    if(!HasAuthority()) return;

    if (OtherActor != nullptr && OtherActor != this && OtherActor != GetInstigator())
    {
        UGameplayStatics::ApplyDamage(OtherActor, Damage, GetInstigatorController(), this, DamageType);
        ReplicateHit(OtherActor, Result);
    }
}

void AProjectileBase::ReplicateHit_Implementation(AActor* OtherActor, const FHitResult& Result)
{
    if (HitParticle)
        UGameplayStatics::SpawnEmitterAtLocation(this, HitParticle, GetActorLocation(), FRotator::ZeroRotator);

    if (HitSound) UGameplayStatics::PlaySoundAtLocation(this, HitSound, GetActorLocation());

    if (HitShake)
    {
        for(FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
        {
            APlayerController* Controller = Iterator->Get();
            if(!Controller) continue;
            if(Controller->IsLocalController()) Controller->PlayerCameraManager->PlayCameraShake(HitShake, 1);
        }
    }
    if (AStaticMeshActor* Mesh = Cast<AStaticMeshActor>(OtherActor))
    {
        FString Prop = FString(TEXT("Prop"));
        UStaticMeshComponent* MeshComponent = Mesh->GetStaticMeshComponent();
        if (MeshComponent && HasPropTag(Mesh))
        {
            Mesh->SetMobility(EComponentMobility::Movable);
            MeshComponent->SetSimulatePhysics(true);
            if(ProjectileMesh)
                MeshComponent->AddForceAtLocation(Result.Normal * -1 * MovementSpeed * ImpactForce, Result.ImpactPoint);
        }
    }
    Destroy();
}

bool AProjectileBase::ReplicateHit_Validate(AActor* OtherActor, const FHitResult& Result)
{
    return true;
}

bool AProjectileBase::HasPropTag(AStaticMeshActor* MeshActor)
{
    if(!MeshActor) return false;

    const FString Prop = FString(TEXT("Prop"));
    for (auto Tag : MeshActor->Tags)
    {
        if(Tag.ToString() != Prop) continue;
        return true;
    }
    return false;
}

// Called when the game starts or when spawned
void AProjectileBase::BeginPlay()
{
    Super::BeginPlay();
    if (LaunchSound) UGameplayStatics::PlaySoundAtLocation(this, LaunchSound, GetActorLocation());
}
