// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProjectileBase.generated.h"

class UParticleSystemComponent;
class UProjectileMovementComponent;
class AStaticMeshActor;
class UCameraShake;

UCLASS()
class TOONTANKS_API AProjectileBase : public AActor
{
    GENERATED_BODY()

public:
    AProjectileBase();

private:
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    UProjectileMovementComponent* ProjectileMovement;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    UStaticMeshComponent* ProjectileMesh;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    UParticleSystemComponent* ParticleTrail;

    UPROPERTY(EditDefaultsOnly, Category = "Damage", meta = (AllowPrivateAccess = "true"))
    TSubclassOf<UDamageType> DamageType;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage", meta = (AllowPrivateAccess = "true"))
    float Damage = 50.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Damage", meta = (AllowPrivateAccess = "true"))
    float ImpactForce = 1.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite,Category = "Movement", meta = (AllowPrivateAccess = "true"))
    float MovementSpeed = 1300.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite,Category = "Effects", meta = (AllowPrivateAccess = "true"))
    UParticleSystem* HitParticle;
    UPROPERTY(EditAnywhere, BlueprintReadWrite,Category = "Effects", meta = (AllowPrivateAccess = "true"))
    USoundBase* HitSound;
    UPROPERTY(EditAnywhere, BlueprintReadWrite,Category = "Effects", meta = (AllowPrivateAccess = "true"))
    USoundBase* LaunchSound;
    UPROPERTY(EditAnywhere, BlueprintReadWrite,Category = "Effects", meta = (AllowPrivateAccess = "true"))
    TSubclassOf<UCameraShake> HitShake;

    UFUNCTION()
    void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherCompo, FVector NormalImpulse,
               const FHitResult& Result);
    UFUNCTION(NetMulticast, UnReliable, WithValidation)
    void ReplicateHit(AActor* OtherActor, const FHitResult& Result);
    static bool HasPropTag(AStaticMeshActor* MeshActor);

protected:
    virtual void BeginPlay() override;
};
