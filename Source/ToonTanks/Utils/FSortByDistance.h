﻿#pragma once

struct FSortByDistance
{
    explicit FSortByDistance(const FVector& InSourceLocation)
        : SourceLocation(InSourceLocation)
    {

    }

    FVector SourceLocation;

    bool operator()(const AActor& A, const AActor& B) const
    {
        const float DistanceA = FVector::DistSquared(SourceLocation, A.GetActorLocation());
        const float DistanceB = FVector::DistSquared(SourceLocation, B.GetActorLocation());

        return DistanceA > DistanceB;
    }
};
