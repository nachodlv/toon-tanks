// Fill out your copyright notice in the Description page of Project Settings.


#include "TankGameModeBase.h"

#include "Kismet/GameplayStatics.h"
#include "ToonTanks/Pawn/PawnTank.h"
#include "ToonTanks/Pawn/PawnTurret.h"
#include "ToonTanks/PlayerControllers/PlayerControllerBase.h"
#include "ToonTanks/PlayerStates/PlayerStateBase.h"
#include "Engine/Classes/GameFramework/PlayerStart.h"

void ATankGameModeBase::BeginPlay()
{
    Super::BeginPlay();
    TargetTurrets = GetTargetTurretCount();
    if(PlayerStarts.Num() == 0) InitializePlayerStarts();
}

void ATankGameModeBase::StartMatch()
{
    Super::StartMatch();
    HandleGameStart();
}

bool ATankGameModeBase::ReadyToStartMatch_Implementation()
{
    return MaxPlayers == NumPlayers;
}


void ATankGameModeBase::ActorDie(AActor* DeadActor, AActor* InstigatedBy)
{
    if (APawnTank* PawnTank = Cast<APawnTank>(DeadActor))
    {
        HandleLose(PawnTank);
        if (APlayerControllerBase* PlayerController = Cast<APlayerControllerBase>(PawnTank->GetController()))
        {
            PlayerController->SetPlayerEnabledState(false);
        }
    }
    if (APawnTurret* DestroyedTurret = Cast<APawnTurret>(DeadActor))
    {
        if (APawn* Pawn = Cast<APawn>(InstigatedBy))
        {
            if (APlayerStateBase* PlayerState = Cast<APlayerStateBase>(Pawn->GetPlayerState()))
            {
                PlayerState->EnemyDestroyed(DestroyedTurret->GetScore());
            }
        }
        TargetTurrets--;
        if (TargetTurrets == 0) HandleWin();
    }
}

void ATankGameModeBase::HandleGameStart() const
{
    TArray<APlayerControllerBase*> PlayerControllers;
    GetPlayerControllers(PlayerControllers);
    for (APlayerControllerBase* PlayerController : PlayerControllers)
    {
        PlayerController->SetPlayerEnabledState(false);
        PlayerController->HandleGameStart(StartDelay);
        FTimerHandle PlayerEnableHandle;
        const FTimerDelegate PlayerEnableDelegate = FTimerDelegate::CreateUObject(
            PlayerController, &APlayerControllerBase::SetPlayerEnabledState, true);
        GetWorld()->GetTimerManager().SetTimer(PlayerEnableHandle, PlayerEnableDelegate, StartDelay, false);
    }
}

void ATankGameModeBase::HandleLose(APawnTank* TankDie) const
{
    TArray<APlayerControllerBase*> PlayerControllers;
    GetPlayerControllers(PlayerControllers);
    for (APlayerControllerBase* PlayerController : PlayerControllers)
    {
        if (PlayerController->GetPawn() != TankDie) continue;
        PlayerController->HandleGameOver(false);
    }
}

void ATankGameModeBase::HandleWin() const
{
    TArray<APlayerControllerBase*> PlayerControllers;
    GetPlayerControllers(PlayerControllers);
    for (APlayerControllerBase* PlayerController : PlayerControllers)
    {
        PlayerController->HandleGameOver(true);
    }
}


int32 ATankGameModeBase::GetTargetTurretCount() const
{
    const TSubclassOf<APawnTurret> ClassToFind = APawnTurret::StaticClass();
    TArray<AActor*> TurretActors;
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), ClassToFind, TurretActors);
    return TurretActors.Num();
}

void ATankGameModeBase::GetPlayerControllers(TArray<APlayerControllerBase*>& PlayerControllers) const
{
    FConstPlayerControllerIterator PlayerControllersIterator = GetWorld()->GetPlayerControllerIterator();
    for (; PlayerControllersIterator; ++PlayerControllersIterator)
    {
        if (APlayerControllerBase* PlayerController = Cast<APlayerControllerBase>(PlayerControllersIterator->Get()))
        {
            PlayerControllers.Add(PlayerController);
        }
    }
}

void ATankGameModeBase::InitializePlayerStarts()
{
    // Get all player starts in the world
    TArray<AActor*> PlayerStartsFound;
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerStart::StaticClass(), PlayerStartsFound);
    PlayerStarts.Reserve(PlayerStartsFound.Num());
    for (AActor* StartFound : PlayerStartsFound)
    {
        if(APlayerStart* PlayerStart = Cast<APlayerStart>(StartFound)) PlayerStarts.Add(PlayerStart);
    }
}

AActor* ATankGameModeBase::ChoosePlayerStart_Implementation(AController* Player)
{
    if(PlayerStarts.Num() == 0)
    {
        InitializePlayerStarts();
    }
    LastSpawn = (LastSpawn + 1) % PlayerStarts.Num();
    return PlayerStarts[LastSpawn];
}
