// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/GameMode.h"
#include "TankGameModeBase.generated.h"

class APlayerControllerBase;
class APawnTank;
class APawnTurret;
class UGameInstanceBase;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FEnemyDestroyed, int, Score);
UCLASS()
class TOONTANKS_API ATankGameModeBase : public AGameMode
{
    GENERATED_BODY()

protected:
    virtual void BeginPlay() override;

public:
    void ActorDie(AActor* DeadActor, AActor* InstigatedBy);

private:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Game Loop", meta=(AllowPrivateAccess="true"))
    int StartDelay = 1;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Game Loop", meta=(AllowPrivateAccess="true"))
    int MaxPlayers = 2;

    UPROPERTY()
    TArray<APlayerStart*> PlayerStarts;

    int32 LastSpawn = -1;
    int32 TargetTurrets = 0;


    virtual void StartMatch() override;
    virtual bool ReadyToStartMatch_Implementation() override;
    virtual AActor* ChoosePlayerStart_Implementation(AController* Player) override;

    void HandleGameStart() const;
    void HandleLose(APawnTank* TankDie) const;
    void HandleWin() const;
    int32 GetTargetTurretCount() const;
    void GetPlayerControllers(TArray<APlayerControllerBase*>& PlayerControllers) const;
    void InitializePlayerStarts();
};
