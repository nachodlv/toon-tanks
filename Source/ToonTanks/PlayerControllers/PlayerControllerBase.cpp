// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerControllerBase.h"

#include "ToonTanks/Hud/HUDBase.h"
#include "ToonTanks/Pawn/PawnShooter.h"
#include "ToonTanks/Actors/ProjectileBase.h"
#include "ToonTanks/Components/ScoreDisplayer.h"
#include "ToonTanks/Components/HealthDisplayer.h"

void APlayerControllerBase::HandleGameStart_Implementation(int StartDelay)
{
    if (AHUDBase* Hud = Cast<AHUDBase>(GetHUD()))
        Hud->GameStart(StartDelay);
}

bool APlayerControllerBase::HandleGameStart_Validate(int StartDelay)
{
    return true;
}

void APlayerControllerBase::HandleGameOver_Implementation(bool playerWon)
{
    if (AHUDBase* Hud = Cast<AHUDBase>(GetHUD()))
        Hud->GameOver(playerWon);
}

bool APlayerControllerBase::HandleGameOver_Validate(bool playerWon)
{
    return true;
}

void APlayerControllerBase::SetPlayerEnabledState_Implementation(bool SetPlayerEnabled)
{
    APawn* CurrentPawn = GetPawn();
    if (!CurrentPawn) return;
    if (SetPlayerEnabled)
    {
        CurrentPawn->EnableInput(this);
        bShowMouseCursor = true;
    }
    else
    {
        CurrentPawn->DisableInput(this);
        bShowMouseCursor = false;
    }
}

bool APlayerControllerBase::SetPlayerEnabledState_Validate(bool SetPlayerEnabled)
{
    return true;
}

void APlayerControllerBase::SpawnProjectile_Implementation(FVector Location, FRotator Rotation,
                                                           TSubclassOf<AProjectileBase> ProjectileClass)
{
    FActorSpawnParameters SpawnParameters;
    SpawnParameters.Instigator = GetPawn();
    GetWorld()->SpawnActor<AProjectileBase>(ProjectileClass, Location, Rotation, SpawnParameters);
}

bool APlayerControllerBase::SpawnProjectile_Validate(FVector Location, FRotator Rotation,
                                                     TSubclassOf<AProjectileBase> ProjectileClass)
{
    return true;
}

void APlayerControllerBase::TickActor(const float DeltaTime, const ELevelTick TickType,
                                      FActorTickFunction& ThisTickFunction)
{
    Super::TickActor(DeltaTime, TickType, ThisTickFunction);
    if (!IsLocalController()) return;

    APawnShooter* CurrentPawn = Cast<APawnShooter>(GetPawn());
    if (!CurrentPawn) return;
    FHitResult TraceHitResult;
    GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
    const FVector HitLocation = TraceHitResult.ImpactPoint;
    CurrentPawn->RotateTurret(HitLocation);
}

void APlayerControllerBase::InitPlayerState()
{
    Super::InitPlayerState();
}

void APlayerControllerBase::OnRep_PlayerState()
{
    Super::OnRep_PlayerState();
    InitializeDisplayers();
}

void APlayerControllerBase::OnPossess(APawn* InPawn)
{
    Super::OnPossess(InPawn);
    if (IsNetMode(NM_ListenServer) && IsLocalController())
    {
        InitializeDisplayers();
    }
}

void APlayerControllerBase::InitializeDisplayers()
{
    // Setup Score Combo
    if(ScoreDisplayerClass)
    {
        UScoreDisplayer* ScoreCombo = NewObject<UScoreDisplayer>(this, ScoreDisplayerClass);
        if (ScoreCombo) ScoreCombo->PlayerControllerPossessed(this);
    }

    // Setup the Health Displayer component
    if(HealthDisplayerClass)
    {
        UHealthDisplayer* HealthDisplayer = NewObject<UHealthDisplayer>(this, HealthDisplayerClass);
        if (HealthDisplayer) HealthDisplayer->PlayerControllerPossessed(this);
    }
}
