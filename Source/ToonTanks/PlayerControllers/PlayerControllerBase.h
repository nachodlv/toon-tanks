// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "ToonTanks/Actors/ProjectileBase.h"
#include "PlayerControllerBase.generated.h"

class AProjectileBase;
class UHealthDisplayer;
class UScoreDisplayer;

UCLASS()
class TOONTANKS_API APlayerControllerBase : public APlayerController
{
    GENERATED_BODY()

public:
    UFUNCTION(Client, Reliable, WithValidation)
    void SetPlayerEnabledState(bool SetPlayerEnabled);
    UFUNCTION(Client, Reliable, WithValidation)
    void HandleGameOver(bool playerWon);
    UFUNCTION(Client, Reliable, WithValidation)
    void HandleGameStart(int StartDelay);
    UFUNCTION(Server, Unreliable, WithValidation)
    void SpawnProjectile(FVector Location, FRotator Rotation, TSubclassOf<AProjectileBase> ProjectileClass);

private:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Component Classes", meta = (AllowPrivateAccess = "true"))
    TSubclassOf<UScoreDisplayer> ScoreDisplayerClass;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Component Classes", meta = (AllowPrivateAccess = "true"))
    TSubclassOf<UHealthDisplayer> HealthDisplayerClass;

    virtual void TickActor(float DeltaTime, enum ELevelTick TickType, FActorTickFunction& ThisTickFunction) override;
    virtual void InitPlayerState() override;
    virtual void OnRep_PlayerState() override;
    virtual void OnPossess(APawn* InPawn) override;

    void InitializeDisplayers();
};
