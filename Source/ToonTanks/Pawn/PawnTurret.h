// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PawnShooter.h"

#include "PawnTurret.generated.h"

class APawnTank;
UCLASS()
class TOONTANKS_API APawnTurret : public APawnShooter
{
    GENERATED_BODY()
public:
    APawnTurret();
    int GetScore() const;

protected:
    virtual void BeginPlay() override;
    virtual void HandleDestruction() override;

private:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement", meta = (AllowPrivateAccess = "true"))
    float FireRate = 2.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement", meta = (AllowPrivateAccess = "true"))
    float FireRange = 500.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Score", meta = (AllowPrivateAccess = "true"))
    int Score = 100;

    FTimerHandle FireRateTimeHandle;
    bool PlayerInRange = false;

    virtual void Tick(float DeltaTime) override;
    void CheckFireCondition();
    APawnTank* GetPlayerInRange();
};
