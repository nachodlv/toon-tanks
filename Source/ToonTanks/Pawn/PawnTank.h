// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PawnShooter.h"
#include "ToonTanks/Components/HaveCheckpointArrow.h"


#include "PawnTank.generated.h"

class USpringArmComponent;
class UCameraComponent;
class UCheckpointArrow;
class APlayerControllerBase;

UCLASS()
class TOONTANKS_API APawnTank : public APawnShooter, public IHaveCheckpointArrow
{
    GENERATED_BODY()
public:
    APawnTank();
    bool GetPlayerAlive() const;
    virtual UCheckpointArrow* GetCheckpointArrow() override;

protected:
    virtual void BeginPlay() override;
    virtual void HandleDestruction() override;
    virtual void PawnClientRestart() override;
    virtual void Fire() override;

private:
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    USpringArmComponent* SpringArm;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    UCameraComponent* Camera;
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components", meta = (AllowPrivateAccess = "true"))
    UCheckpointArrow* CheckpointArrow;
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category= "Components", meta = (AllowPrivateAccess = "true"))
    UStaticMeshComponent* CheckpointArrowMesh;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement", meta = (AllowPrivateAccess = "true"))
    float MoveSpeed = 100.f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Movement", meta = (AllowPrivateAccess = "true"))
    float RotateSpeed = 100.f;

    FVector MoveDirection;
    FQuat RotationDirection;
    UPROPERTY()
    APlayerControllerBase* PlayerControllerRef;
    FHitResult TraceHitResult;
    bool bAlive = true;

    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    void CalculateMoveInput(float Value);
    void CalculateRotateInput(float Value);
};
