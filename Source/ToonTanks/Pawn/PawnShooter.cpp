// Fill out your copyright notice in the Description page of Project Settings.

#include "PawnShooter.h"
#include "Kismet/KismetMathLibrary.h"
#include "../Actors/ProjectileBase.h"


APawnShooter::APawnShooter()
{
    BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Base Mesh"));
    BaseMesh->SetupAttachment(RootComponent);

    TurretMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Turret Mesh"));
    TurretMesh->SetupAttachment(BaseMesh);

    ProjectileSpawnPoint = CreateDefaultSubobject<USceneComponent>(TEXT("Projectile Spawn Point"));
    ProjectileSpawnPoint->SetupAttachment(TurretMesh);
}

void APawnShooter::RotateTurret_Implementation(FVector LookAtTarget)
{
    const FVector StartLocation = TurretMesh->GetComponentLocation();
    const FRotator TurretRotation = UKismetMathLibrary::FindLookAtRotation(
        StartLocation, FVector(LookAtTarget.X, LookAtTarget.Y, TurretMesh->GetComponentLocation().Z));
    TurretMesh->SetWorldRotation(TurretRotation);
}

bool APawnShooter::RotateTurret_Validate(FVector LookAtTarget)
{
    return true;
}

void APawnShooter::Fire()
{
    if (ProjectileClass)
    {
        const FVector SpawnLocation = ProjectileSpawnPoint->GetComponentLocation();
        const FRotator SpawnRotation = TurretMesh->GetComponentRotation();
        FActorSpawnParameters SpawnParameters;
        SpawnParameters.Instigator = this;
        GetWorld()->SpawnActor<AProjectileBase>(ProjectileClass, SpawnLocation, SpawnRotation, SpawnParameters);
    }
}
