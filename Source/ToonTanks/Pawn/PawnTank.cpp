// Fill out your copyright notice in the Description page of Project Settings.


#include "PawnTank.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/PlayerController.h"
#include "Camera/CameraComponent.h"
#include "ToonTanks/Components/CheckpointArrow.h"
#include "ToonTanks/PlayerControllers/PlayerControllerBase.h"

APawnTank::APawnTank()
{
    PrimaryActorTick.bCanEverTick = true;

    SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("Spring arm"));
    SpringArm->SetupAttachment(RootComponent);

    Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
    Camera->SetupAttachment(SpringArm);

    CheckpointArrow = CreateDefaultSubobject<UCheckpointArrow>(TEXT("Checkpoint Arrow"));
    CheckpointArrow->SetupAttachment(RootComponent);

    CheckpointArrowMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Checkpoint Arrow Mesh"));
    CheckpointArrowMesh->SetupAttachment(CheckpointArrow);
    CheckpointArrow->SetArrowStaticMesh(CheckpointArrowMesh);
}

bool APawnTank::GetPlayerAlive() const
{
    return bAlive;
}

UCheckpointArrow* APawnTank::GetCheckpointArrow()
{
    return CheckpointArrow;
}

// Called when the game starts or when spawned
void APawnTank::BeginPlay()
{
    Super::BeginPlay();
    PlayerControllerRef = Cast<APlayerControllerBase>(GetController());
}

void APawnTank::HandleDestruction()
{
    Super::HandleDestruction();

    bAlive = false;
    SetActorHiddenInGame(true);
    SetActorTickEnabled(false);
}

void APawnTank::PawnClientRestart()
{
    Super::PawnClientRestart();
    PlayerControllerRef = Cast<APlayerControllerBase>(GetController());
}

void APawnTank::Fire()
{
    if (ProjectileClass && PlayerControllerRef)
    {
        const FVector SpawnLocation = ProjectileSpawnPoint->GetComponentLocation();
        const FRotator SpawnRotation = TurretMesh->GetComponentRotation();
        PlayerControllerRef->SpawnProjectile(SpawnLocation, SpawnRotation, ProjectileClass);
    }
}

// Called to bind functionality to input
void APawnTank::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);
    PlayerInputComponent->BindAxis("MoveForward", this, &APawnTank::CalculateMoveInput);
    PlayerInputComponent->BindAxis("Turn", this, &APawnTank::CalculateRotateInput);
    PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &APawnTank::Fire);
}

void APawnTank::CalculateMoveInput(const float Value)
{
    const FVector Forward = GetActorForwardVector();
    AddMovementInput(Forward * Value);
}

void APawnTank::CalculateRotateInput(const float Value)
{
    AddControllerYawInput(Value);
}

