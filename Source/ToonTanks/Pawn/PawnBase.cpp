// Fill out your copyright notice in the Description page of Project Settings.


#include "PawnBase.h"
#include "Kismet/GameplayStatics.h"
#include "ToonTanks/Components/HealthComponent.h"
#include "ToonTanks/Components/Destructible.h"

// Sets default values
APawnBase::APawnBase()
{
    // Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = false;

    HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health Component"));
    if(HealthComponent && HasAuthority())
    {
        HealthComponent->OnHealthUpdated.AddDynamic(this, &APawnBase::HealthUpdated);
    }
    DestructibleComponent = CreateDefaultSubobject<UDestructible>(TEXT("Destructible Component"));
}

void APawnBase::PawnDestroyed()
{
    HandleDestruction();
}

UHealthComponent* APawnBase::GetHealthComponent()
{
    return HealthComponent;
}

void APawnBase::HandleDestruction()
{
    if(DestructibleComponent) DestructibleComponent->Destroy();
}

void APawnBase::HealthUpdated(float CurrentHealth)
{
    if(CurrentHealth == 0) PawnDestroyed();
}
