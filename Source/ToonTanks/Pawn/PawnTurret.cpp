// Fill out your copyright notice in the Description page of Project Settings.


#include "PawnTurret.h"
#include "PawnTank.h"
#include "Kismet/GameplayStatics.h"
#include "ToonTanks/Utils/FSortByDistance.h"

APawnTurret::APawnTurret()
{
    PrimaryActorTick.bCanEverTick = true;
}

int APawnTurret::GetScore() const
{
    return Score;
}

void APawnTurret::BeginPlay()
{
    Super::BeginPlay();

    GetWorld()->GetTimerManager().SetTimer(FireRateTimeHandle, this, &APawnTurret::CheckFireCondition, FireRate, true,
                                           false);
}

void APawnTurret::HandleDestruction()
{
    Super::HandleDestruction();
    Destroy();
}

void APawnTurret::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    if (!HasAuthority()) return;
    if (APawnTank* Player = GetPlayerInRange()) RotateTurret(Player->GetActorLocation());
}

void APawnTurret::CheckFireCondition()
{
    if (!PlayerInRange) return;
    Fire();
}


APawnTank* APawnTurret::GetPlayerInRange()
{
    const FVector ActorLocation = GetActorLocation();
    const TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;
    const TArray<AActor*> ActorsToIgnore;
    TArray<AActor*> PlayersInRange;
    UKismetSystemLibrary::SphereOverlapActors(GetWorld(), ActorLocation, FireRange, ObjectTypes,
                                              APawnTank::StaticClass(), ActorsToIgnore, PlayersInRange);
    PlayersInRange.Sort(FSortByDistance(ActorLocation));
    if(PlayersInRange.Num() > 0)
    {
        if(APawnTank* PawnTank = Cast<APawnTank>(PlayersInRange[0]))
        {
            if(PawnTank->GetPlayerAlive())
            {
                PlayerInRange = true;
                return PawnTank;
            }
        }
    }

    PlayerInRange = false;
    return nullptr;

}
