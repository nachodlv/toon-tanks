// Fill out your copyright notice in the Description page of Project Settings.


#include "PawnProp.h"
#include "ToonTanks/Components/Destructible.h"
#include "ToonTanks/Components/HealthComponent.h"


APawnProp::APawnProp()
{
    PrimaryActorTick.bCanEverTick = false;
    HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health Component"));
    if (HealthComponent && HasAuthority())
    {
        HealthComponent->OnHealthUpdated.AddDynamic(this, &APawnProp::HealthUpdated);
    }
    DestructibleComponent = CreateDefaultSubobject<UDestructible>(TEXT("Destructible Component"));
}

void APawnProp::HealthUpdated(const float CurrentHealth)
{
    if (CurrentHealth == 0 && DestructibleComponent)
    {
        DestructibleComponent->Destroy();
        TArray<AActor*> ChildActors;
        GetAllChildActors(ChildActors, true);
        for (AActor* Child : ChildActors) Child->Destroy();
        Destroy();
    }
}
