// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Character.h"
#include "GameFramework/Pawn.h"
#include "ToonTanks/Components/HaveHealthComponent.h"


#include "PawnBase.generated.h"

class UCapsuleComponent;
class UHealthComponent;
class UCameraShake;

class UDestructible;
UCLASS()
class TOONTANKS_API APawnBase : public ACharacter, public IHaveHealthComponent
{
    GENERATED_BODY()

public:
    APawnBase();
    void PawnDestroyed();
    virtual UHealthComponent* GetHealthComponent() override;

protected:
    virtual void HandleDestruction();

private:
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    UHealthComponent* HealthComponent;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
    UDestructible* DestructibleComponent;

    UFUNCTION()
    void HealthUpdated(float CurrentHealth);
};
