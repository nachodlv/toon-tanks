// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "ToonTanks/Pawn/PawnBase.h"
#include "PawnProp.generated.h"

class UHealthComponent;
class UDestructible;
UCLASS()
class TOONTANKS_API APawnProp : public AActor
{
	GENERATED_BODY()
public:
	APawnProp();
private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UHealthComponent* HealthComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
	UDestructible* DestructibleComponent;

	UFUNCTION()
	void HealthUpdated(float CurrentHealth);
};
