// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "PlayerStateBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FUpdateMultiplier, int, Multiplier);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FUpdateScore, float, Score);
UCLASS()
class TOONTANKS_API APlayerStateBase : public APlayerState
{
    GENERATED_BODY()

public:
    APlayerStateBase();

    FUpdateMultiplier OnUpdateMultiplier;
    FUpdateScore OnUpdateScore;

    int GetMultiplier() const;
    float GetTimeToLoseCombo() const;
    void EnemyDestroyed(float EnemyScore);
    float GetComboTimeRemaining() const;

private:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Score", meta=(AllowPrivateAccess="true"))
    int StartingMultiplier = 1;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Score", meta=(AllowPrivateAccess="true"))
    float TimeToLoseCombo = 1;

    UPROPERTY(ReplicatedUsing=OnRep_Multiplier)
    int Multiplier = 1;
    UPROPERTY()
    float ComboTimeRemaining;

    virtual void Tick(float DeltaSeconds) override;

    UFUNCTION()
    void OnRep_Multiplier() const;
    UFUNCTION(NetMulticast, Reliable, WithValidation)
    void RestartTimeRemaining();
    virtual void OnRep_Score() override;
};
