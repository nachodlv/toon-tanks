// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerStateBase.h"

#include "Net/UnrealNetwork.h"

APlayerStateBase::APlayerStateBase()
{
    PrimaryActorTick.bCanEverTick = true;
    bAlwaysRelevant = true;
}

int APlayerStateBase::GetMultiplier() const
{
    return Multiplier;
}

float APlayerStateBase::GetTimeToLoseCombo() const
{
    return TimeToLoseCombo;
}

void APlayerStateBase::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
    if(Multiplier <= 1) return;

    ComboTimeRemaining -= DeltaSeconds;

    if(!HasAuthority()) return;
    if (ComboTimeRemaining <= 0)
    {
        Multiplier --;
        OnUpdateMultiplier.Broadcast(Multiplier);
        if (Multiplier > 1) RestartTimeRemaining();
    }
}

void APlayerStateBase::OnRep_Multiplier() const
{
    OnUpdateMultiplier.Broadcast(Multiplier);
}

void APlayerStateBase::RestartTimeRemaining_Implementation()
{
    ComboTimeRemaining = TimeToLoseCombo;
}

bool APlayerStateBase::RestartTimeRemaining_Validate()
{
    return true;
}

void APlayerStateBase::OnRep_Score()
{
    OnUpdateScore.Broadcast(GetScore());
}

void APlayerStateBase::EnemyDestroyed(const float EnemyScore)
{
    RestartTimeRemaining();
    SetScore(GetScore() + EnemyScore * Multiplier);
    Multiplier ++;
    OnUpdateMultiplier.Broadcast(Multiplier);
    OnUpdateScore.Broadcast(GetScore());
}

float APlayerStateBase::GetComboTimeRemaining() const
{
    return ComboTimeRemaining;
}

void APlayerStateBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);
    DOREPLIFETIME(APlayerStateBase, Multiplier);
}
